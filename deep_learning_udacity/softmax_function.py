#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Created by Chukwunyere Igbokwe on September 28, 2016 by 5:35 PM

import matplotlib.pyplot as plt
import numpy as np

"""Softmax."""

scores = [3.0, 1.0, 0.2]


def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    return np.exp(x)/np.sum(np.exp(x),axis=0)
# TODO: Compute and return softmax(x)



# print softmax(scores)
# print softmax(scores * 10)
print sum(softmax(scores))
s = np.array([3.0, 1.0, 0.2])
print softmax(s/10) # probabilities get close to uniform distribution
print softmax(s*10)
# Plot softmax curves

x = np.arange(-2.0, 6.0, 0.1)
# print x
scores = np.vstack([x, np.ones_like(x), 0.2 * np.ones_like(x)])
# print scores
plt.plot(x, softmax(scores).T, linewidth=2)
# plt.plot(x, label='x')
# plt.plot( softmax(scores).T, linewidth=2,label='softmax(scores)')
plt.legend()
plt.ylim(0,1.0)
plt.show()


