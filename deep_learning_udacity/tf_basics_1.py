#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Created by Chukwunyere Igbokwe on December 20, 2016 by 5:39 PM

import tensorflow as tf

x1 = tf.constant(5)
x2 = tf.constant(6)

# result = x1 * x2
result = tf.mul(x1,x2)
summation = tf.add(x1,x2)

# print result

# sess = tf.Session()
# print sess.run(result)
# sess.close()

with tf.Session() as sess:
    output = sess.run(result)
    print  sess.run(output)

# print o