#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created by Chukwunyere Igbokwe on July 12, 2016 by 5:20 PM

import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


class Neural_Network(object):

    def __init__(self, Lambda=0):
        #Define HyperParameters
        self.inputLayerSize = 2
        self.outputLayerSize = 1
        self.hiddenLayerSize = 3
        # Weights
        self.W1 = np.random.randn(self.inputLayerSize, self.hiddenLayerSize)
        self.W2 = np.random.randn(self.hiddenLayerSize, self.outputLayerSize)
        # Regularization Parameter:
        self.Lambda = Lambda

    def sigmoid(self,z):
        '''
        :param z:
        :return: 1/(1 + np.exp(-z))
        '''
        return 1/(1 + np.exp(-z))

    def sigmoid_prime(self,z):
        return np.exp(-z)/((1+np.exp(-z))**2)

    def forward(self,X):
        '''
        Propagete inputs through network
        :param x:
        :return:yHat
        '''
        self.z2 = np.dot(X, self.W1)
        self.a2 = self.sigmoid(self.z2)
        self.z3 = np.dot(self.a2, self.W2)
        yHat = self.sigmoid(self.z3)
        return yHat

    def costFunction(self,X,y):
        '''
        Compute cost for given X, y, use weights already stored in class
        :param X:
        :param y:
        :return:J
        '''
        self.yHat = self.forward(X)
        J = 0.5*sum((y-self.yHat)**2)/X.shape[0] + (self.Lambda/2) * sum((sum(self.W1**2)+sum(self.W2**2)))
        return J

    def costFunctionPrime(self,X,y):
        '''
        Compute derivative with respect to W1 and W2 for a given X and y
        :param X:
        :param y:
        :return:dJdW1,dJdW2
        '''
        self.yHat = self.forward(X)
        delta3 = np.multiply(-(y - self.yHat), self.sigmoid_prime(self.z3))
        # Add gradient of regularization term:
        dJdW2 = np.dot(self.a2.T, delta3) / X.shape[0] + self.Lambda * self.W2
        # print 'dJdW2', dJdW2
        delta2 = np.dot(delta3, self.W2.T) * self.sigmoid_prime(self.z2)
        # Add gradient of regularization term:
        dJdW1 = np.dot(X.T, delta2) / X.shape[0] + self.Lambda * self.W1
        # print 'dJdW1', dJdW1
        return dJdW1, dJdW2

    # Helper functions for interacting with other methods/classes
    def getParams(self):
        '''
        Get W1 and W2 rolled into a vector
        :return:params
        '''
        params = np.concatenate((self.W1.ravel(),self.W2.ravel()))
        return params

    def setParams(self,params):
        '''
        Set W1 and W2 using single parameter vector
        :param params:
        :return:
        '''
        W1_start = 0
        W1_end = self.hiddenLayerSize*self.inputLayerSize
        self.W1 = np.reshape(params[W1_start:W1_end], (self.inputLayerSize,self.hiddenLayerSize))
        W2_end = W1_end + self.hiddenLayerSize*self.outputLayerSize
        self.W2 = np.reshape(params[W1_end:W2_end],(self.hiddenLayerSize, self.outputLayerSize))

    def computeGradients(self,X,y):
        dJdW1,dJdW2 = self.costFunctionPrime(X,y)
        return np.concatenate((dJdW1.ravel(),dJdW2.ravel()))

def computeNumericalGradient(N, X, y):
    paramsInitial = N.getParams()
    numgrad = np.zeros(paramsInitial.shape)
    perturb = np.zeros(paramsInitial.shape)
    e = 1e-4

    for p in range(len(paramsInitial)):
        # Set perturbation vector
        perturb[p] = e
        N.setParams(paramsInitial + perturb)
        loss2 = N.costFunction(X, y)

        N.setParams(paramsInitial - perturb)
        loss1 = N.costFunction(X, y)

        # Compute Numerical Gradient
        numgrad[p] = (loss2 - loss1) / (2 * e)

        # Return the value we changed to zero:
        perturb[p] = 0

    # Return Params to original value:
    N.setParams(paramsInitial)

    return numgrad


class trainer(object):
    def __init__(self, N):
        # Make Local reference to network:
        self.N = N

    def callbackF(self, params):
        '''
        For tracking J as we train the network
        :param params:
        :return:
        '''
        self.N.setParams(params)
        self.J.append(self.N.costFunction(self.X, self.y))
        self.testJ.append(self.N.costFunction(self.testX, self.testY))

    def costFunctionWrapper(self, params, X, y):
        self.N.setParams(params)
        cost = self.N.costFunction(X, y)
        grad = self.N.computeGradients(X, y)
        return cost, grad

    def train(self, trainX, trainY, testX, testY):
        # Make an internal variable for the callback function:
        self.X = trainX
        self.y = trainY
        self.testX = testX
        self.testY = testY
        # Make empty list to store costs:
        self.J = []
        self.testJ = []
        # initial parameters and options
        params0 = self.N.getParams()
        options = {'maxiter': 200, 'disp': True}
        #BFGS
        _res = optimize.minimize(self.costFunctionWrapper, params0, jac=True, method='BFGS', args=(trainX, trainY), options=options, callback=self.callbackF)
        #Replace original parameters with trained parameters
        self.N.setParams(_res.x)
        self.optimizationResults = _res

def main():
    #Training Data:trainX = (hours sleeping, hours studying), trainY = score on test
    trainX = np.array(([[3,5], [5,1], [10,2], [6,1.5]]), dtype=float)
    trainY = np.array(([[75], [82], [93], [70]]), dtype=float)
    #Testing Data:
    testX = np.array(([[4, 5.5], [4.5,1], [9,2.5], [6, 2]]), dtype=float)
    testY = np.array(([[70], [89], [85], [75]]), dtype=float)

    #Normalize:
    trainX = trainX/np.amax(trainX, axis=0)
    trainY = trainY/100 #Max test score is 100
    #Normalize by max of training data:
    testX = testX/np.amax(trainX, axis=0)
    testY = testY/100 #Max test score is 100

    #Plot projections of our new data:
    fig0 = plt.figure(0,(8,3))
    fig0.suptitle("Scatter Plot of Hours Sleeping and Hours Studying vs Test Scores")
    plt.subplot(1,2,1)
    plt.scatter(trainX[:,0], trainY)
    plt.grid(1)
    plt.xlabel('Hours Sleeping')
    plt.ylabel('Test Score')

    plt.subplot(1,2,2)
    plt.scatter(trainX[:,1], trainY)
    plt.grid(1)
    plt.xlabel('Hours Studying')
    plt.ylabel('Test Score')

    NN= Neural_Network(Lambda=0.001)
    yHat = NN.forward(trainX)
    print "untrained test results\n",yHat

    numgrad = computeNumericalGradient(NN, trainX, trainY)
    grad = NN.computeGradients(trainX, trainY)
    #Should be less than 1e-8:
    if np.linalg.norm(grad-numgrad)/np.linalg.norm(grad+numgrad) < 1e-8:
        print "We're good"
    else:
        print "Oops!"


    T = trainer(NN)
    T.train(trainX, trainY,testX,testY)
    yHat_train = NN.forward(trainX)
    print "trained test results\n", yHat_train


    #Test network for various combinations of sleep/study:
    hoursSleep = np.linspace(0, 10, 100)
    hoursStudy = np.linspace(0, 5, 100)

    #Normalize data (same way training data was normalized)
    hoursSleepNorm = hoursSleep/10.
    hoursStudyNorm = hoursStudy/5.

    #Create 2-d version of input for plotting
    a, b = np.meshgrid(hoursSleepNorm, hoursStudyNorm)

    #Join into a single input matrix:
    allInputs = np.zeros((a.size, 2))
    allInputs[:, 0] = a.ravel()
    allInputs[:, 1] = b.ravel()

    fig1 = plt.figure(1)
    fig1.suptitle("Comparing Testing and Training Data")
    plt.subplot(211)
    plt.plot(T.J)
    plt.plot(T.testJ)
    plt.xlabel("Iterations")
    plt.ylabel("Cost")
    plt.legend(['Training', 'Testing'])
    plt.grid()

    allOutputs = NN.forward(allInputs)
    #Contour Plot:
    yy = np.dot(hoursStudy.reshape(100,1), np.ones((1,100)))
    xx = np.dot(hoursSleep.reshape(100,1), np.ones((1,100))).T

    plt.subplot(212)
    CS = plt.contour(xx,yy,100*allOutputs.reshape(100, 100))
    plt.clabel(CS, inline=1, fontsize=10)
    plt.xlabel('Hours Sleep')
    plt.ylabel('Hours Study')
    #3-D Plot
    fig2 = plt.figure(2)
    fig2.suptitle("3D Plot of Random Hours Slept,Hours Studied and Test Scores")
    ax = fig2.gca(projection = '3d')
    ax.scatter(10*trainX[:,0], 5*trainX[:,1], 100*trainY, c='k', alpha = 1, s=30)
    surf = ax.plot_surface(xx,yy,100*allOutputs.reshape(100, 100), cmap=plt.cm.jet, alpha = 0.5)
    ax.set_xlabel('Hours Sleep')
    ax.set_ylabel('Hours Study')
    ax.set_zlabel('Test Score')
    plt.show()

if __name__=='__main__':
    main()